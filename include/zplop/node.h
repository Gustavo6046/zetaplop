/*****************************************************/
/*          _            __            _       _     */
/*  ____ __| |___ _ __  / / _  ___  __| |___  | |_   */
/* |_ / '_ \ / _ \ '_ \/ / ' \/ _ \/ _` / -_)_| ' \  */
/* /__| .__/_\___/ .__/_/|_||_\___/\__,_\___(_)_||_| */
/*    |_|        |_|                                 */
/* Pathnode definitions.                             */
/*****************************************************/
// Authored by Gustavo Rehermann <rehermann6046@gmail.com>.
// Licensed under MIT.

#ifndef NODE_H
#define NODE_H

#include "zplop/lists.h"



typedef enum ZB_NodeType {
    Z_NT_NORMAL = 0,
    Z_NT_USE,
    Z_NT_SLOW,
    Z_NT_CROUCH,
    Z_NT_JUMP,
    Z_NT_AVOID,
    Z_NT_SHOOT,
    Z_NT_RESPAWN,
    Z_NT_TARGET //! should not be allowed to be plopped.
} ZB_NodeType;


typedef struct ZB_PathNode {
    long x, y, z;
    ZB_NodeType type;
    unsigned char angle;
} ZB_PathNode;


typedef struct ZB_PathNodeList {
    ZB_PathNode *node;

    struct ZB_PathNodeList *prev;
    struct ZB_PathNodeList *next;
} ZB_PathNodeList;


typedef struct ZB_GameNodeList {
    unsigned short mapName_size;
    char *mapName_ptr;

    ZB_PathNodeList *nodes;
    struct ZB_GameNodeList *next;
    struct ZB_GameNodeList *prev;
} ZB_GameNodeList;



/*********************************************************************/
/* Glue code for the common linked list interface ("zplop/lists.h"). */
/*********************************************************************/

void ZB_PathNodeList_Advancer(ZB_LinkedListView *list, ZB_LinkedListIterator *iter);
void ZB_GameNodeList_Advancer(ZB_LinkedListView *list, ZB_LinkedListIterator *iter);



#endif // node.h